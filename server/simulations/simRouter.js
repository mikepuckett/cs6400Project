(function() {
  'use strict';

  const router = require('express').Router();

  const simulationRunner = require('./simulationRunner.js');

  const simulation1 = require('./simulations.json');

  router.get('/sim/:number', (req, res) => {
    const simNumber = req.params.number;
    simulationRunner
        .start(simulation1[simNumber])
        .then(simResult => {
          res.status(200).send(simResult);
        })
        .catch(err => {
          res.status(500).end();
        })
  });

  module.exports = router;
})();