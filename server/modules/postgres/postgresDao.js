(function() {
  'use strict';

  const pg = require('pg');
  const constants = require('../constants.js');
  const Promise = require('promise');

  const connectionPool = new pg.Pool({
    user: constants.POSTGRES_USER,
    database: constants.POSTGRES_DATABASE,
    host: constants.POSTGRES_HOST,
    port: constants.POSTGRES_PORT,
    max: constants.MAX_POOL_SIZE,
    min: constants.MIN_POOL_SIZE,
    idleTimeoutMillis: 1000
  });

  /*
   * This will hold a reference to the "listener" callback function provided by the service
   * that subscribes to the database notifications. This value is set in `this.#listen`
   */
  var notifyServiceFunc;

  connectionPool
      .connect()
      .then(client => {
        client.on('notification', msg => {
          if (notifyServiceFunc)
            notifyServiceFunc(JSON.parse(msg.payload));
        });
        client.query('LISTEN new_bid');
      })
      .catch(err => {
        throw err;
      });

  function queryForList(query) {
    return new Promise((resolve, reject) => {
      connectionPool
          .query(query)
          .then(result => {
            resolve(result.rows);
          })
          .catch(err => {
            console.error(err);
            reject(err);
          });
    });
  }

  function queryForEntity(query) {
    return new Promise((resolve, reject) => {
      connectionPool
          .query(query)
          .then(result => {
            resolve(result.rows[0])
          })
          .catch(err => {
            console.error(err);
            reject(err);
          });
    });
  }

  function addEntity(query) {
    return new Promise((resolve, reject) => {
      connectionPool
          .query(query)
          .then(result => {
            resolve(result.rows[0]);
          })
          .catch(err => {
            // console.error(err);
            reject(err);
          });
    });
  }

  module.exports = {

    /**
     * Adds a new user
     * @param user {Object} - the user object to add
     * @param user.name {String} - the userName of the user. Must be unique
     * @returns {Promise}
     */
    addUser: function addUser(user) {
      const query = {
        text: `
          INSERT INTO project.user VALUES ($1) RETURNING "userName"
        `,
        values: [
          user.userName
        ]
      };

      return addEntity(query);
    },

    /**
     * Adds a new item
     * @param item {Object} - the item object to add
     * @param item.itemName {String} - the name of the item
     * @returns {Promise}
     */
    addItem: function addItem(item) {
      const query = {
        text: `
            INSERT INTO project.item VALUES ($1) RETURNING "itemName"
        `,
        values: [
          item.itemName
        ]
      };
      return addEntity(query)
    },

    /**
     * Allows a user to watch a specific item
     * @param userName {String} - the userName of the user who wishes to watch
     * an item
     * @param itemName {String} - the itemName of the item to be watched
     * @returns {Promise}
     */
    addUserWatch: function addUserWatch(userName, itemName) {
      const query = {
        text: `
           INSERT
             INTO project.userWatch
           VALUES ($1, $2)
        `,
        values: [
          userName,
          itemName
        ]
      };

      return addEntity(query);
    },

    addBid: function(userName, itemName, price) {
      const query = {
        text: `
          SELECT project.submitNewBid($1, $2, $3)
        `,
        values: [
          price,
          itemName,
          userName
        ]
      };

      return addEntity(query);
    },

    /**
     * Rerturns a list of all bids
     * @returns {Promise}
     */
    getBids: function getBids() {
      const query = `
          SELECT *
            FROM project.bid
      `;

      return queryForList(query);
    },

    /**
     * Returns a list of all items in the database
     * @returns {Promise}
     */
    getItems: function getItems() {
      const query = `
          SELECT *
            FROM project.item
      `;

      return queryForList(query);
    },

    /**
     * Returns a list of all users in the database
     * @returns {Promise}
     */
    getUsers: function getUsers() {
      const query = `
          SELECT *
            FROM project.user
      `;

      return queryForList(query);
    },

    /**
     * Gets a single item by itemName
     * @param itemName {String} - the id of the item to get
     * @returns {Promise} - resolves to the item object
     */
    getItem: function getItem(itemName) {
      const query = {
        text: `
          SELECT *
            FROM project.item
           WHERE "itemName" = $1
        `,
        values: [
          itemName
        ]
      };
      return queryForEntity(query);
    },

    /**
     * Subscribes to the new bid notification stream
     * @param listenCallback {Function} - callback function that is called for each new notification
     */
    listen: function(listenCallback) {
      notifyServiceFunc = listenCallback;
    },

    ensureListenerInitiated: function() {
      // noop. The rethinkDao needs this function so its added here for equivalence between
      // the two DAOs
    }

  };
})();