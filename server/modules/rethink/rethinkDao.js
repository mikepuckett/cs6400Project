(function() {
  'use strict';

  const Promise = require('promise');

  const constants = require('../constants.js');

  const r = require('rethinkdbdash')({
    host: constants.RETHINK_HOST,
    port: constants.RETHINK_PORT,
    db: constants.RETHINK_DATABASE,
    max: constants.MAX_POOL_SIZE,
    buffer: constants.MIN_POOL_SIZE
  });

  /*
   * This will hold a reference to the "listener" callback function provided by the service
   * that subscribes to the database notifications. This value is set in `this.#listen`
   */
  var notifyServiceFunc;

  function subscribe() {
    r.table('items')
        .changes()
        .run()
        .then(cursor => {
          cursor.each((err, row) => {

            if (!row) {
              return;
            }
            
            const newBids = row.new_val.bids || [];

            if (newBids.length === 0) {
              return;
            }

            // sort the bid array by price to get the maximum price
            const maxBid = newBids.sort((a, b) => b.price - a.price)[0];
            if (notifyServiceFunc) {
              maxBid.itemName = row.new_val.itemName;
              notifyServiceFunc(maxBid);
            }

          });
        })
        .catch(err => {
          throw err;
        });
  }

  function queryForList(tableName) {
    return new Promise((resolve, reject) => {
      r.db(constants.RETHINK_DATABASE)
          .table(tableName)
          .run()
          .then(result => {
            resolve(result);
          })
          .catch(err => {
            reject(err);
          });
    });
  }

  function queryForEntity(tableName, entityId) {
    return new Promise((resolve, reject) => {
      r.table(tableName)
          .get(entityId)
          .run()
          .then(result => {
            resolve(result);
          })
          .catch(err => {
            reject(err);
          });
    });
  }

  function addEntity(entity, tableName) {
    return new Promise((resolve, reject) => {
      r.table(tableName)
          .insert(entity, { returnChanges: true })
          .run()
          .then(result => {
            resolve(result.changes[0].new_val);
          })
          .catch(err => {
            reject(err);
          });
    });
  }

  function updateEntity(tableName, entityId, update) {
    return new Promise((resolve, reject) => {
      r.table(tableName)
          .get(entityId)
          .update(update, { returnChanges: true })
          .then(result => {
            resolve(result.changes[0].new_val)
          })
          .catch(err => {
            reject(err);
          });
    });
  }

  function roundNumber(number) {
    return Math.round(number * 100) / 100;
  }

  function submitBid(userName, itemName, price) {
    return new Promise((resolve, reject) => {
      r.table('items')
          .get(itemName)
          .update(row => {
            // r.branch(a, b, c) => { IF a THEN b ELSE c }
            return r.branch(
              // IF the submitted price is higher than the maximum bidded price
              row('bids').default([])('price').max().default(0).lt(price),
              // THEN append the new bid to the bids array
              { bids: row('bids').default([]).append({ userName: userName, price: roundNumber(price) }) },
              // ELSE the bids array should stay unchanged
              { bids: row('bids') }
            );
          })
          .run()
          .then(result => {
            resolve();
          })
          .catch(err => {
            console.error(err);
            reject(err);
          });
    });
  }

  module.exports = {

    /**
     * Adds a new user to the database
     * @param user {Object} - the user object to add
     * @param user.userName {String} - the userName of the user. Must be unique
     * @returns {Promise}
     */
    addUser: function addUser(user) {
      return addEntity(user, 'users');
    },

    /**
     * Adds a new item
     * @param item {Object} - the item object to add
     * @param item.itemName {String} - the name of the item
     * @returns {Promise}
     */
    addItem: function addItem(item) {
      return addEntity(item, 'items');
    },

    /**
     * Allows a user to watch a specific item
     * @param userName {String} - the userName of the user who wishes to watch an item
     * @param itemName {String} - the itemName of the item to be watched
     * @returns {Promise}
     */
    addUserWatch: function addUserWatch(userName, itemName) {
        return updateEntity(
          'users',
          userName,
          {
              watching: r.row('watching').default([]).append(itemName)
          }
        );
    },

    addBid: function(userName, itemName, price) {
      return submitBid(userName, itemName, price);
    },

    /**
     * Resolves a list of all items
     * @returns {Promise}
     */
    getItems: function getItems() {
      return queryForList('items');
    },

    /**
     * Returns a list of all users
     * @returns {Promise}
     */
    getUsers: function getUsers() {
      return queryForList('users');
    },

    /**
     * Gets a single item by itemName
     * @param itemName {String} - the id of the item to get
     * @returns {Promise} - resolves to the item object
     */
    getItem: function (itemName) {
      return queryForEntity('items', itemName);
    },

    /**
     * Subscribes to the new bid notification stream
     * @param listenCallback {Function} - callback function that is called for each new notification
     */
    listen: function(listenCallback) {
      notifyServiceFunc = listenCallback;
    },

    /**
     * Called to subscribe to a changefeed regarding items.
     */
    ensureListenerInitiated: function() {
      subscribe();
    }
  };
})();