(function() {
  module.exports = {

    // MUST BE TWO!!!! One connection gets used to "listen" for updates
    MIN_POOL_SIZE: 100,

    // The maximum database connection pool size
    MAX_POOL_SIZE: 100,

    // postgres connection info
    POSTGRES_USER: 'postgres',
    POSTGRES_DATABASE: 'postgres',
    POSTGRES_HOST: 'postgres',
    POSTGRES_PORT: 5432,

    // rethink connection info
    RETHINK_DATABASE: 'project',
    RETHINK_HOST: 'rethink',
    RETHINK_PORT: 28015,

    BID_INCREMENT: .05
  };
})();