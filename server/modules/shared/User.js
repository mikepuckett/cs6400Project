(function() {
  'use strict';

  const constants = require('../constants.js');

  const BID_INCREMENT = constants.BID_INCREMENT;
  const openTimeouts = {};

  var User = function User(userName, maxBidDelay) {
    this.userName = userName;
    this.maxBidDelay = maxBidDelay;
    this.itemWatches = {};
  };

  User.prototype.watchItem = function(item, bidFunc) {

    var me = this;

    me.bidFunc = bidFunc;

    me.itemWatches[item.itemName] = item;

    me.respondToBid(item.itemName, item.initialPrice);
  };

  /**
   * Allows the user object to respond to a new bid. Possible "actions" can include ignoring
   * the bid (either if the bid was by this user, or if the bid price is higher than the user
   * specific maximum price for the item), or the user can submit a new bid.
   * @param bid {Object} - the bid that will be responded to
   * @param bid.userName {String} - the username of the user who made the bid
   * @param bid.itemName {String} - the name if the item that is being bid on
   * @param bid.price {Number} - the price of the bid
   */
  User.prototype.respondToBid = function(itemName, price, userName) {
    var me = this;

    if (userName === me.userName) {
      return;
    }

    if (openTimeouts[itemName]) {
      clearTimeout(openTimeouts[itemName]);
      delete openTimeouts[itemName];
    }

    openTimeouts[itemName] = setTimeout(() => {
      me.bidFunc(me.userName, itemName, price + BID_INCREMENT);
      // delay the bid up to a configurable amount (in ms) to simulate real-world variability in response times.
    }, (Math.random() * (this.maxBidDelay || 0)) + 1);
  }

  module.exports = User;
})();