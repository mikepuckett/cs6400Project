(function() {
  'use strict';

  module.exports = function(dependencies) {

    const dao = dependencies.dao;
    const Promise = require('promise');

    /*
     * The following five variables are used to hold in-memory information that is used
     * for the simulation.
     */
    var inMemoryItems = {}; // item object representations, keyed by itemName
    var itemToUserWatch = {}; // map (keyed by itemName) of all users who are watching a particular item
    var openBids = {}; // map (keyed by itemName) of all open bids (submitted not yet accepted) by users
    var bidsAcceptedCount = 0; // the total number of bids that have been accepted
    var totalAcceptedTime = 0; // the aggregated delay (in ms) of all accepted bids

    // used to hold function references for a promise that needs to be resolved externally from the promise.
    var signalSimulationEnd;
    var signalSimulationError;

    /*
     * Configure the service to listen for notifications from the DAO
     */
    dao.listen(bid => {

      if (!openBids[bid.itemName]) {
        // this catches a potential bug that occasionally occurs due to a race condition. Sometimes,
        // a new bid notification may be received after the auction has ended.
        return;
      }

      // keep a count of notifications that will be used for statistical purposes
      bidsAcceptedCount++;

      /*
       * Find the time that the accepted bid was submitted, and get the time (now) that the
       * notification was accepted.
       */
      const bidSubmittedTime = openBids[bid.itemName][bid.userName];
      const bidAcceptedTime = new Date().getTime();

      /*
       * Calculate the delay from when the bid was submitted to when it was accepted
       */
      const delay = bidAcceptedTime - bidSubmittedTime;

      /*
       * Keep a running tally of the total delay of all accepted bids for the simulation.
       * This is used for statistical purposes.
       */
      totalAcceptedTime += delay;

      console.log(`${bid.userName} bid for ${bid.itemName} at ${bid.price} accepted in ${delay}ms`);

      /*
       * Clear out the open bids for this particular item to prepare for the next round of bids
       * that will follow a newly accepted bid.
       */
      openBids[bid.itemName] = {};

      /*
       * If there are no users watching this item, then don't do anything else. This can happen
       * if a notification is received after an item's auction time limit has already expired (at
       * which point all user watches are removed). 
       */
      if (!itemToUserWatch[bid.itemName]) {
        return;
      }

      const userWatchArray = itemToUserWatch[bid.itemName];
      const arrLen = userWatchArray.length

      // randomly sort the array of users so there is a little randomness in the order users
      // are iterated over
      userWatchArray.sort((a, b) => 100 * Math.random());

      // split the array into 5 sub-arrays in an attempt to add extra concurrency in user bids
      const splitUserWatchArrays = [
        userWatchArray.slice(0, arrLen / 5),
        userWatchArray.slice(arrLen / 5, 2 + arrLen / 5),
        userWatchArray.slice(2 * arrLen / 5, 3 * arrLen / 5),
        userWatchArray.slice(3 * arrLen / 5, 4 * arrLen / 5),
        userWatchArray.slice(4 * arrLen / 5, arrLen)
      ];

      // iterate over the 5 sub arrays to notify users of the bid
      let i, j, arr;
      for (i = 0; i < 5; i++) {
        arr = splitUserWatchArrays[i];
        for (j = 0; j < arr.length; j++) {
          arr[j].respondToBid(bid.itemName, bid.price, bid.userName);
        }
      }
    });

    /**
     * Once the time limit of all item auctions has expired, this function is called to calculate statistics
     * and signal (to the simulation runner) that the simulation is over.
     */
    function teardownSimulation() {

      setTimeout(() => {
        const averageDelay = totalAcceptedTime / bidsAcceptedCount;
        console.log(`Simulation ended with a total of ${bidsAcceptedCount} accepted bids`);
        console.log('Average notification delay = ', averageDelay);

        signalSimulationEnd({
          totalBids: bidsAcceptedCount,
          averageDelay: averageDelay
        });
      }, 100);
    }

    /*
     * All functions within this return object are exposed on the interface of this service to be called externally
     */
    return {

      /**
       * Adds a new item
       * @param item {Object} - the item object to add
       * @param item.itemName {String} - the name of the item
       * @returns {Promise}
       */
      addItem: function addItem(item) {
        return new Promise((resolve, reject) => {
          dao.addItem(item)
              .then(result => {
                
                /*
                 * Create a placeholder for open bids that will be made for this item.
                 */
                openBids[item.itemName] = {};

                /*
                 * Create a placeholder for users that will watch this particular item.
                 */
                itemToUserWatch[result.itemName] = [];

                /*
                 * Save this item object in memory so it can be accessed easily for the simulation.
                 */
                inMemoryItems[result.itemName] = item;

                resolve(result);
              })
              .catch(err => {
                reject(err);
              });
        });
      },

      /**
       * Adds a new user
       * @param user {Object} - the user object to add
       * @param user.name {String} - the userName of the user. Must be unique
       * @returns {Promise}
       */
      addUser: function addUser(user) {
        return dao.addUser(user);
      },

      /**
       * Configures a user to watch an item, and then gets the date/time when
       * the item goes on auction.
       * @param userName {String} - the user who is watching an item
       * @param itemName {String} - the item that will be watched
       * @returns {Promise} - resolves to the item
       */
      addUserWatch: function addUserWatch(user, itemName) {
        return new Promise((resolve, reject) => {
          dao.addUserWatch(user.userName, itemName)
              .then(result => {
                
                dao.getItem(itemName)
                    .then(result => {

                      itemToUserWatch[itemName].push(user);

                      resolve({ userName: user.userName, item: result })
                    })
                    .catch(err => {
                      reject(err);
                    });
              })
              .catch(err => {
                reject(err);
              });
        });
      },

      /**
       * Gets a list of all items in the system
       * @returns {Promise} - resolves a list of items
       */
      getAllItems: function getAllItems() {
        return dao.getItems();
      },

      /**
       * Gets a list of all users in the system
       * @returns {Promise} - resolves a list of users
       */
      getAllUsers: function getAllUsers() {
        return dao.getUsers()
      },

      /**
       * Gets a single item by itemName
       * @param itemName {String} - the id of the item to get
       * @returns {Promise} - resolves to the item object
       */
      getItem: function getItem(itemName) {
        return dao.getItem(itemName);
      },

      /**
       * Submits a bid. There is no guarantee that this bid will be accepted. If this bid loses a
       * race condition with another bid for the same item, it will simply be ignored.
       * @param userName {String} - the userName that is responsible for submitting the bid.
       * @param itemName {String} - the item that is being bid on
       * @param price {Number} - the price of the submitted bid.
       */
      submitBid: function(userName, itemName, price) {

        if (!openBids[itemName]) {
          // this catches a potential bug that occasionally occurs due to a race condition. Sometimes,
          // this function may be called after an auction has already ended.
          return;
        }
        /**
         * Capture the time that this bid was submitted. This is used to determine the total delay
         * of receiving a notification of an accepted bid.
         */
        openBids[itemName][userName] = new Date().getTime();

        console.log(`${userName} submitting bid for ${itemName} at ${price}`);
        dao.addBid(userName, itemName, price);
      },

      /**
       * Called from the simulation runner to kick off the simulation. This should be called after the simulation
       * has been set up (all users added, all items added, and all user watches added).
       * @returns {Promise} - resolves once all auctions have expired (by per-item configured time limit)
       */
      startSimulation: function() {

        dao.ensureListenerInitiated();

        var me = this;

        /*
         * For each item that has been added, open/start the auction, and then cofigure the ending (determined
         * by per-item configured time limit).
         */
        Object.keys(inMemoryItems).forEach(itemName => {

          /*
           * Notify all users who are watching the item that the auction has begun
           */
          itemToUserWatch[itemName].forEach(user => {
            user.watchItem(inMemoryItems[itemName], me.submitBid);
          });
          
          /*
           * Set a timeout (based on the item's auction time limit) that will end the auction.
           */
          setTimeout(() => {

            /*
             * Once the timelimit has expired, remove all in memory user watches so that users will no
             * longer be notified of accepted bids
             */
            delete itemToUserWatch[itemName];

            /*
             * If this item was the last remaining item with an open auction, then end the simulation.
             */
            if (Object.keys(itemToUserWatch).length === 0) {
              teardownSimulation();
            }

            // set the timeout for the configured item specific time limit
          }, inMemoryItems[itemName].auctionTimeLimit);
        });

        return new Promise((resolve, reject) => {
          signalSimulationEnd = resolve;
          signalSimulationError = reject;
        });
      },

      /**
       * Clears out all cached simulation information
       */
      resetSimulation: function() {
        inMemoryItems = {};
        itemToUserWatch = {};
        openBids = {};
        bidsAcceptedCount = 0;
        totalAcceptedTime = 0;
      }
    };
  }
})();