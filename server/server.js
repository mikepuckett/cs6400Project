(function() {
  'use strict';

  const express = require('express');
  const bodyParser = require('body-parser');

  const PORT = 8080;

  const app = express();
  app.use(bodyParser.json());

  const postgresRouter = require('./modules/postgres/postgresRouter.js');
  app.use('/postgres', postgresRouter);

  const rethinkRouter = require('./modules/rethink/rethinkRouter.js');
  app.use('/rethink', rethinkRouter);

  const simulationRouter = require('./simulations/simRouter.js');
  app.use('/simulation', simulationRouter);

  app.get('/', function(req, res) {
    res.send('Hello World!\n');
  });

  app.listen(PORT);

  console.log('Listening on port: ', PORT);
})();