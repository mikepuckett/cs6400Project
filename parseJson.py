import json
import sys

with open('.jsonResults.json') as json_file:
  data = json.load(json_file)

jsonKeys = sys.argv[1].split('.')

if len(jsonKeys) == 1:
  print data[jsonKeys[0]]
elif len(jsonKeys) == 2:
  print data[jsonKeys[0]][jsonKeys[1]]