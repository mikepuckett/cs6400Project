#!/bin/bash

SIM_FILE_NAME="simResults/simResults.$(date +"%Y%m%d.%T").csv"
echo 'numUsers,numItems,maxBidDelay,auctionTimeLimit,postgresAverageDelay,rethinkAverageDelay,postgresTotalBids,rethinkTotalBids' > ${SIM_FILE_NAME}

rebuildDatabases () {
  echo '\nRebuilding databases...'
  psql -h localhost -d postgres -U postgres -f postgres/scripts/rebuildDb.sql
  node rethink/scripts/rebuildDb.js
  echo 'Finished rebuilds\n'
}

getJsonValue () {
  python parseJson.py $1
}

buildResultRow () {
  echo $(getJsonValue numUsers),$(getJsonValue numItems),$(getJsonValue maxBidDelay),$(getJsonValue auctionTimeLimit),$(getJsonValue postgres.averageDelay),$(getJsonValue rethink.averageDelay),$(getJsonValue postgres.totalBids),$(getJsonValue rethink.totalBids)
}

runSim () {
  rebuildDatabases
  echo 'Kicking off simulation ' $1
  curl -XGET 'localhost:31013/simulation/sim/'$1 -o .jsonResults.json
  buildResultRow >> ${SIM_FILE_NAME}
}

# Make the assumtion that the containers are already up and running
echo 'Assuming that all containers are running...'

runSim 1
runSim 2
runSim 3
runSim 4
runSim 5
runSim 6
runSim 7
runSim 8

# final rebuild, otherwise the databases take long enought to start up that the current
# docker-compose start up process fails
rebuildDatabases

