DROP SCHEMA IF EXISTS project CASCADE;

CREATE SCHEMA project;

CREATE TABLE project.USER
(
  "userName" VARCHAR(20) PRIMARY KEY NOT NULL
);

CREATE TABLE project.ITEM
(
  "itemName" VARCHAR(20) PRIMARY KEY NOT NULL
);

CREATE TABLE project.BID
(
  "bidId" SERIAL PRIMARY KEY,
  "userName" VARCHAR(20) REFERENCES project.USER ("userName"),
  "itemName" VARCHAR(20) REFERENCES project.ITEM ("itemName"),
  "price" NUMERIC(10, 2) NOT NULL,
  UNIQUE ("itemName", "price")
);

CREATE TABLE project.USERWATCH
(
  "userName" VARCHAR(20) REFERENCES project.USER ("userName"),
  "itemName" VARCHAR(20) REFERENCES project.ITEM ("itemName"),
  PRIMARY KEY ("userName", "itemName")
);

CREATE VIEW project.MAX_BID AS (
    SELECT DISTINCT ON("itemName")
           "itemName",
           "bidId",
           "userName",
           price
      FROM project.BID
  ORDER BY "itemName", price DESC
);

CREATE OR REPLACE FUNCTION project.newBidNotify() RETURNS trigger AS $$
DECLARE
  bid json;
BEGIN

  bid = (SELECT row_to_json(MAX_BID)
           FROM project.MAX_BID
          WHERE "itemName" = NEW."itemName"
          LIMIT 1
        );

  PERFORM pg_notify('new_bid', bid::text);
  RETURN NEW;

END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER new_bid_trigger AFTER INSERT ON project.BID
FOR EACH ROW EXECUTE PROCEDURE project.newBidNotify();

CREATE OR REPLACE FUNCTION project.submitNewBid(NUMERIC, TEXT, TEXT) RETURNS VOID AS $$
DECLARE
  max_price NUMERIC;
BEGIN
  
  max_price = (SELECT price FROM project.MAX_BID WHERE "itemName"::text = $2);

  IF max_price IS NULL THEN
    INSERT
      INTO project.BID ("userName", "itemName", "price")
    VALUES ($3, $2, $1);
  ELSEIF $1 > max_price THEN
    INSERT
      INTO project.BID ("userName", "itemName", "price")
    VALUES ($3, $2, $1);
  END IF;

END;
$$ LANGUAGE plpgsql;
